from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404, render

from .models import Contenido

@csrf_exempt
def index(request):
    content_list = Contenido.objects.all()[:5]
    context = {'content_list': content_list}
    return render(request, 'cms/index.html', context)

@csrf_exempt
def edit(request, llave):
    if request.method == "POST":
        action = request.POST['action']
        if action == "Enviar Contenido":
            valor = request.POST['valor']
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    context = {'contenido': contenido}
    return render(request, 'cms/edit.html', context)

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        
        try:
            respuesta = Contenido.objects.get(clave=llave).valor
            c = Contenido.objects.get(clave=llave, valor=respuesta).delete()
            c = Contenido(clave=llave, valor=valor)
      
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
            
        c.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    context = {'contenido': contenido}
    return render(request, 'cms/content.html', context)


@csrf_exempt
def get_css(request, llave):

    if request.method == "PUT":
        valor = request.body.decode('utf-8')

        try:
            respuesta = Contenido.objects.get(clave=llave).valor
            c = Contenido.objects.get(clave=llave, valor=respuesta).delete()
            c = Contenido(clave=llave, valor=valor)

        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)

        c.save()

    contenido = get_object_or_404(Contenido, clave=llave)
    context = {'contenido': contenido}
    return render(request, 'cms/css.html', context)


