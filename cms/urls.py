from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('<str:llave>', views.get_content),
    path('edit/<str:llave>', views.edit),
    path('css/<str:llave>', views.get_css),
]
